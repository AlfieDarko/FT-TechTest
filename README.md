FT Software Engineer Tech Test
==================
Deployed Here >> https://ft-techtest.herokuapp.com/<br><br>

# Images<br>

<img src="https://preview.ibb.co/c3tatd/Screen_Shot_2018_07_16_at_12_16_53.png" alt="Screen_Shot_2018_07_16_at_12_16_53" border="0"><br><br>
<img src="https://preview.ibb.co/f9qzKJ/Screen_Shot_2018_07_16_at_12_17_17.png" alt="Screen_Shot_2018_07_16_at_12_17_17" border="0">
<br><br>
<img src="https://preview.ibb.co/fGWgzJ/Screen_Shot_2018_07_16_at_12_25_39.png" alt="Screen_Shot_2018_07_16_at_12_25_39" border="0"></a>

Thoughts & Approach
-------
I first made a list of reading material for the unfamiliar front-end concepts so I could get a better understanding of what this project entailed. Having had time constraints due to other technical tests and interviews all at the same time, I set an aim of completing the functionality of the site and implementing new technology in order so I can get familiar with new tech in the short space of time. This was my first time working with gulp and compiling SCSS to CSS so it was a great research excercise.

I will use this project as a base to continue my research and develop my skills with.

Overall I was pleased I was able to create my own pagination and used the API to offset the page results. I thought about adding in a database to paginate the results much easily from there but since this was just a small app to return search results, I opted against it. <br> It was more challenging and a better learning experience achieving this with the FT API. <br>

I would loved to have done so much more testing, especially end-to-end. I spent alot of time trying to use nock to mock the api calls in the tests (First time using Nock)  & spent alot of time trying to get it to work properly. <b>In the end, I made a choice to atleast try and get something done so that I can deploy rather than strict testing. Due to time constraints, I felt that it was the best choice & the choice that would allow me to learn the most in a short timespan.</b>

User Stories
-------
>As a user,
<br> so that I can quickly learn about a subject
<br> I want to be able to search FT.com headlines

>As a user,
<br> so that I'm not overburdened by information
<br> I want to be able to view only 20 search results per page

>As a user,
<br> so that I can check the news wherever I am
<br> I want to the site to be vieweable on all my devices


Instructions
-------
Javascript<br>
Node <br>
Express <br>
Gulp <br>

ESlint<br>
Mocha <br>
Nock <br>
SuperTest <br>
TestCafe <br>

Deployed on heroku! (https://ft-techtest.herokuapp.com/)


Instructions
-------

* Clone this repository
* Enter into the repo folder
* Run '```npm install```'
* Run '```npm start```'

Testing
* Run '```npm test```'
* Run '```npm featuretest```'


Known Issues
-------
* Bundle.CSS / JS not working on deployed site for some reason! I have CDN as backup but not enough time to fully investigate the issue under time allowance. <B>Unfortunately this means the styling isnt as nice as it looked locally (the screenshots).</b> I will be investigating this in the meantime.

* Testing not extensive enough, especially feature testing! I had opted for functionality but this is a project I will do again in my spare time so I can focus more on the process based building.

* Not fully responsive. 

TO-DO
-------
* If I had more time, I would love to have been able to implement the service workers and caching results feature. 
* More research on Grunt and SCSS. Those are two things I will be researching extensively.
* More planning, especially more diagrams and business flow diagrams to illustrate knowledge of systems architecture on this level.
* Relied on more custom CSS as a fallback, especially as I spent alot of time trying to get the origami components to work with the grunt build tools. It would have saved me alot more time.
* Clean up code base especially the appUtilities class.

<br>
Research Material
-------
READING MATERIAL :
SERVICE WORKERS:<br>
https://github.com/matthew-andrews/workshop-making-it-work-offline/tree/master/05-offline-news
https://medium.freecodecamp.org/how-i-made-my-cms-based-website-work-offline-f34afc393ca8
http://blog.lamplightdev.com/2015/01/06/A-Simple-ServiceWorker-App/

ORIGAMI COMPONENTS:<br>
https://breezy.hr/app/c/financial-times/company/origami.ft.com <br>
http://origami.ft.com/docs/developer-guide/<br>

3G PERFORMANT WEBSITES<br>
https://econsultancy.com/blog/66657-need-for-speed-how-to-optimise-website-performance <br>
https://www.hobo-web.co.uk/your-website-design-should-load-in-4-seconds/<br>
https://hackernoon.com/how-to-radically-improve-your-website-performance-part-1-c728f4e5b08f

HEROKU NODEJS DEPLOYMENT<br>
https://medium.com/@seulkiro/deploy-node-js-app-with-gitlab-ci-cd-214d12bfeeb5

SERVERSIDE RENDERING<br>
https://www.youtube.com/watch?v=8A_aFYz8A1w<br>
https://www.smashingmagazine.com/2016/03/server-side-rendering-react-node-express/

PROGRESSIVELY ENHANCED<br>
https://en.wikipedia.org/wiki/Progressive_enhancement<br>
https://www.gov.uk/service-manual/technology/using-progressive-enhancement<br>
https://www.shopify.co.uk/partners/blog/what-is-progressive-enhancement-and-why-should-you-care<br>
https://medium.com/@adambsilver/progressive-enhancement-explained-simply-32dd1dc9e064<br>
https://github.com/jbmoelker/progressive-enhancement-resources

RESPONSIVE DESIGN<br>
https://responsivedesign.is/<br>
https://medium.com/level-up-web/best-practices-of-responsive-web-design-6da8578f65c4<br>
https://medium.com/swlh/everything-you-need-to-know-about-responsive-web-design-54c2059a7e99<br>

ACCESSIBLE DESIGN
https://medium.com/salesforce-ux/7-things-every-designer-needs-to-know-about-accessibility-64f105f0881b<br>
https://www.washington.edu/doit/what-difference-between-accessible-usable-and-universal-design<br>
https://accessibility.blog.gov.uk/2016/09/02/dos-and-donts-on-designing-for-accessibility/

PAGINATION:<br>
https://github.com/expressjs/express-paginate<br>
https://evdokimovm.github.io/javascript/nodejs/mongodb/pagination/expressjs/ejs/bootstrap/2017/08/20/create-pagination-with-nodejs-mongodb-express-and-ejs-step-by-step-from-scratch.html<br>
https://www.youtube.com/watch?v=LvXFiWlZ300<br>
