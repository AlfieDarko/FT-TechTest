const appUtils = require('../utils/appUtils.js')
const express = require('express')
const router = express.Router()
const axios = require('axios')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Express'
  })
})

router.post('/articles', function (req, res, next) {
  console.log(req.body);
  console.log(req.body.searchBox1);

  appUtils.setSearchBoxValue(req.body.searchBox)
  appUtils.searchArticles(req, res)
})

router.get('/articles', function (req, res, next) {
  let currentPage = parseInt(req.query['page'])
  
  appUtils.searchArticles(req, res, currentPage)
})

module.exports = router