import { Selector } from "testcafe";
fixture`Front End Tests`.page`http://localhost:3000/`

test("article on page 1 is different from article on page 2", async t => {
  let searchBoxForm = Selector("#search-box-form")
  let searchBox = Selector("#search-box")
  let submitSearchButton = Selector("#submit-search-btn")
  let page1article0 = Selector('#page-1-article-0').innerText
  let page2article0 = Selector('#page-2-article-0').innerText
  let page2link = Selector("#page-2-link")

  let page1article0string = `Trump shares ‘very nice note’ from North Korea’s Kim
  Donald Trump has shared the text of a letter he said he received from North Korea’s Kim Jong Un in the wake of their...
  https://www.ft.com/content/ec3ae116-85f3-11e8-a29d-73e3d454535d`
  await t
    .click(searchBoxForm)
    .click(searchBox)
    .typeText(searchBox, 'Trump')
    .click(submitSearchButton)
    .wait(100)
    .click(page2link)
    .expect(page2article0).notEql(page1article0string)
});
