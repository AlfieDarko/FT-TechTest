import { Selector } from "testcafe";
fixture`Front End Tests`.page`http://localhost:3000/`

test("search brings up a list of articles", async t => {
  let searchBoxForm = Selector("#search-box-form")
  let searchBox = Selector("#search-box")
  let submitSearchButton = Selector("#submit-search-btn")
  let page1article0 = Selector('#page-1-article-0').innerText
  let page2article0 = Selector('#page-2-article-0').innerText
  let page2link = Selector("#page-2-link")

  let page1article0string = "Trump shares ‘very nice note’ from North Korea’s Kim"
  await t
    .click(searchBoxForm)
    .click(searchBox)
    .typeText(searchBox, 'Trump')
    .click(submitSearchButton)
    .wait(100)
    .expect(page1article0).contains(page1article0string)
})