module.exports = {
  "query": {
    "queryString": "title:\"Clinton\"",
    "queryContext": {
      "curations": [
        "ARTICLES",
        "BLOGS",
        "PAGES",
        "PODCASTS",
        "VIDEOS"
      ]
    },
    "resultContext": {
      "maxResults": 100,
      "offset": 0,
      "aspects": [
        "title",
        "lifecycle",
        "location",
        "summary",
        "editorial"
      ],
      "contextual": true,
      "highlight": false,
      "suppressDefaultSort": false
    }
  },
  "results": [{
    "indexCount": 965,
    "curations": [
      "ARTICLES",
      "BLOGS",
      "PAGES",
      "PODCASTS",
      "VIDEOS"
    ],
    "results": [{
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "31c68ef4-7930-11e8-af48-190d103e32a4",
        "apiUrl": "https://api.ft.com/content/31c68ef4-7930-11e8-af48-190d103e32a4",
        "title": {
          "title": "Death, Clinton and knee-high boots"
        },
        "lifecycle": {
          "initialPublishDateTime": "2018-07-03T04:00:50Z",
          "lastPublishDateTime": "2018-07-03T04:00:50Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/31c68ef4-7930-11e8-af48-190d103e32a4"
        },
        "summary": {
          "excerpt": "...Republican tax bill, Lewinsky tweeted in response: “Blaming the intern is so 1990s.” By contrast, Clinton is an..."
        },
        "editorial": {
          "subheading": "A second chance would be a fine thing, in life and everything else",
          "byline": "Jenny Lee"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "e56916c8-6f05-11e8-92d3-6c13e5c92914",
        "apiUrl": "https://api.ft.com/content/e56916c8-6f05-11e8-92d3-6c13e5c92914",
        "title": {
          "title": "The President is Missing by Bill Clinton and James Patterson — state of the union"
        },
        "lifecycle": {
          "initialPublishDateTime": "2018-06-15T12:50:29Z",
          "lastPublishDateTime": "2018-06-15T12:50:29Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/e56916c8-6f05-11e8-92d3-6c13e5c92914"
        },
        "summary": {
          "excerpt": "President Bill Clinton is surely unique among James Patterson’s co-authors. Their names are the same size on the cover..."
        },
        "editorial": {
          "subheading": "The former US president adds stardust to a high-concept, if clunky, thriller",
          "byline": "Adam LeBor"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "c92c3ed2-6ff9-11e8-852d-d8b934ff5ffa",
        "apiUrl": "https://api.ft.com/content/c92c3ed2-6ff9-11e8-852d-d8b934ff5ffa",
        "title": {
          "title": "Watchdog finds no bias in FBI handling of Clinton emails"
        },
        "lifecycle": {
          "initialPublishDateTime": "2018-06-14T18:24:02Z",
          "lastPublishDateTime": "2018-06-15T11:15:40Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/c92c3ed2-6ff9-11e8-852d-d8b934ff5ffa"
        },
        "summary": {
          "excerpt": "Former FBI director James Comey acted improperly in his handling of the Hillary Clinton email investigation but was not..."
        },
        "editorial": {
          "subheading": "DoJ report says director Comey acted improperly but was not politically motivated",
          "byline": "Kadhim Shubber in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "bda87cee-4fa4-11e8-a7a9-37318e776bab",
        "apiUrl": "https://api.ft.com/content/bda87cee-4fa4-11e8-a7a9-37318e776bab",
        "title": {
          "title": "US unemployment falls below 4% for first time since Clinton era"
        },
        "lifecycle": {
          "initialPublishDateTime": "2018-05-04T15:05:37Z",
          "lastPublishDateTime": "2018-05-04T15:05:37Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/bda87cee-4fa4-11e8-a7a9-37318e776bab"
        },
        "summary": {
          "excerpt": "The US unemployment rate has fallen below 4 per cent for the first time since Bill Clinton was president, leaving the..."
        },
        "editorial": {
          "subheading": "Disappointing wage growth, however, likely to keep Fed on a gently tightening path",
          "byline": "Sam Fleming in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "6cd4f85a-3c13-11e8-b9f9-de94fa33a81e",
        "apiUrl": "https://api.ft.com/content/6cd4f85a-3c13-11e8-b9f9-de94fa33a81e",
        "title": {
          "title": "Bill Clinton urges Northern Ireland to restore government"
        },
        "lifecycle": {
          "initialPublishDateTime": "2018-04-09T18:16:49Z",
          "lastPublishDateTime": "2018-04-09T21:05:45Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/6cd4f85a-3c13-11e8-b9f9-de94fa33a81e"
        },
        "summary": {
          "excerpt": "Bill Clinton has urged political leaders in Northern Ireland to revive their collapsed government, saying there were..."
        },
        "editorial": {
          "subheading": "Ahern, Blair and Mitchell also speak out on 20th anniversary of Good Friday Agreement",
          "byline": "Arthur Beesley in Dublin"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "6da320fa-11d8-11e8-940e-08320fc2a277",
        "apiUrl": "https://api.ft.com/content/6da320fa-11d8-11e8-940e-08320fc2a277",
        "title": {
          "title": "US gunmakers bet on Hillary Clinton and lost"
        },
        "lifecycle": {
          "initialPublishDateTime": "2018-02-20T05:15:26Z",
          "lastPublishDateTime": "2018-02-20T05:15:26Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/6da320fa-11d8-11e8-940e-08320fc2a277"
        },
        "summary": {
          "excerpt": "...preceded the election of Mr Trump, when it seemed that Hillary Clinton would be elected on a policy platform..."
        },
        "editorial": {
          "subheading": "With laws to curb ownership off the table, an expected sales boost failed to materialise",
          "byline": "Jennifer Bissell-Linsk in New York"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "086b3f72-c90f-11e7-ab18-7a9fb7d6163e",
        "apiUrl": "https://api.ft.com/content/086b3f72-c90f-11e7-ab18-7a9fb7d6163e",
        "title": {
          "title": "Sessions moves to quell furore over Clinton Foundation probe"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-11-14T11:06:05Z",
          "lastPublishDateTime": "2017-11-14T19:20:13Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/086b3f72-c90f-11e7-ab18-7a9fb7d6163e"
        },
        "summary": {
          "excerpt": "...appointing a special counsel to investigate the Clinton Foundation and President Donald Trump’s vanquished election..."
        },
        "editorial": {
          "subheading": "US attorney-general says he will ‘study the facts’ before deciding on special counsel",
          "byline": "Courtney Weaver in Washington and John Murray Brown in London"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "2628649f-c2da-30ab-8a97-19fd20f4143d",
        "apiUrl": "https://api.ft.com/content/2628649f-c2da-30ab-8a97-19fd20f4143d",
        "title": {
          "title": "After Manafort charges, Trump asks why Clinton and Democrats are not ‘the focus’"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-10-30T14:33:59Z",
          "lastPublishDateTime": "2017-10-30T14:33:59Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/2628649f-c2da-30ab-8a97-19fd20f4143d"
        },
        "summary": {
          "excerpt": "Donald Trump has responded on Twitter to charges against his former campaign manager, asking why Hillary Clinton and..."
        },
        "editorial": {
          "byline": "Adam Samson"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "6f32543a-993e-11e7-b83c-9588e51488a0",
        "apiUrl": "https://api.ft.com/content/6f32543a-993e-11e7-b83c-9588e51488a0",
        "title": {
          "title": "What Happened: Hillary Clinton unleashed"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-09-14T15:03:17Z",
          "lastPublishDateTime": "2017-09-14T15:03:17Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/6f32543a-993e-11e7-b83c-9588e51488a0"
        },
        "summary": {
          "excerpt": "...Clinton could return to the stage less than a year after losing to Donald Trump was deemed highly selfish. Such..."
        },
        "editorial": {
          "subheading": "The presidential candidate’s memoir of defeat is written with a zest that would have benefited her lacklustre campaign",
          "byline": "Edward Luce"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "83dfd1c4-98de-11e7-a652-cde3f882dd7b",
        "apiUrl": "https://api.ft.com/content/83dfd1c4-98de-11e7-a652-cde3f882dd7b",
        "title": {
          "title": "Martin Shkreli sent to jail over Hillary Clinton hair post"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-09-14T00:39:47Z",
          "lastPublishDateTime": "2017-09-14T00:39:47Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/83dfd1c4-98de-11e7-a652-cde3f882dd7b"
        },
        "summary": {
          "excerpt": "...ruled he presented a danger to the community because of threatening behaviour towards Hillary Clinton. Shkreli..."
        },
        "editorial": {
          "subheading": "Pharma entrepreneur had offered $5,000 to anyone who could ‘grab a hair’ from her head",
          "byline": "David Crow and Jessica Dye in New York"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "70adfba3-0636-358a-9d4c-8f24c10d6698",
        "apiUrl": "https://api.ft.com/content/70adfba3-0636-358a-9d4c-8f24c10d6698",
        "title": {
          "title": "Martin Shkreli sent to jail after Clinton hair post"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-09-13T22:58:39Z",
          "lastPublishDateTime": "2017-09-13T22:58:39Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/70adfba3-0636-358a-9d4c-8f24c10d6698"
        },
        "summary": {
          "excerpt": "...” targeting Hillary Clinton, among others. On Wednesday in Brooklyn federal court, Judge Kiyo Matsumoto ordered..."
        },
        "editorial": {
          "byline": "Jessica Dye"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "d517e576-65ea-11e7-8526-7b38dcaef614",
        "apiUrl": "https://api.ft.com/content/d517e576-65ea-11e7-8526-7b38dcaef614",
        "title": {
          "title": "Donald Trump Jr welcomed offer of Russian help to damage Clinton"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-07-11T04:40:49Z",
          "lastPublishDateTime": "2017-07-12T03:50:35Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/d517e576-65ea-11e7-8526-7b38dcaef614"
        },
        "summary": {
          "excerpt": "...Clinton that they claimed to have received from the top prosecutor in Russia. “If it’s what you say I love it,” Mr Trump..."
        },
        "editorial": {
          "subheading": "‘I love it’, son responded when told Moscow lawyer had detrimental material on rival",
          "byline": "Demetri Sevastopulo and David J Lynch in Washington and Max Seddon in Moscow"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "28c55ee0-6534-11e7-8526-7b38dcaef614",
        "apiUrl": "https://api.ft.com/content/28c55ee0-6534-11e7-8526-7b38dcaef614",
        "title": {
          "title": "Trump Jr admits meeting Russian lawyer for tips on Clinton"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-07-10T08:29:52Z",
          "lastPublishDateTime": "2017-07-10T14:14:57Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/28c55ee0-6534-11e7-8526-7b38dcaef614"
        },
        "summary": {
          "excerpt": "...helping Mrs Clinton. In a statement released on Sunday evening, Mr Trump Jr said he agreed to meet the lawyer,..."
        },
        "editorial": {
          "subheading": "Aides Kushner and Manafort also attended session during presidential campaign",
          "byline": "Naomi Rovnick"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "a8db3ee2-3726-11e7-bce4-9023f8c0fd2e",
        "apiUrl": "https://api.ft.com/content/a8db3ee2-3726-11e7-bce4-9023f8c0fd2e",
        "title": {
          "title": "FT Health: neglected global killer, Chelsea Clinton, fair drug pricing"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-05-12T22:00:27Z",
          "lastPublishDateTime": "2017-05-12T22:00:27Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/a8db3ee2-3726-11e7-bce4-9023f8c0fd2e"
        },
        "summary": {
          "excerpt": "...carbon emissions. The alternatives would also substantially improve individuals’ health. 3 questions Chelsea Clinton..."
        },
        "editorial": {
          "subheading": "Road safety, the problem with cash donors, healthcare spending in sub-Saharan Africa",
          "byline": "Andrew Jack"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "6de2ce4e-340c-11e7-99bd-13beb0903fa3",
        "apiUrl": "https://api.ft.com/content/6de2ce4e-340c-11e7-99bd-13beb0903fa3",
        "title": {
          "title": "Bill Clinton and crime writer to pen White House thriller"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-05-08T18:24:29Z",
          "lastPublishDateTime": "2017-05-08T18:24:29Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/6de2ce4e-340c-11e7-99bd-13beb0903fa3"
        },
        "summary": {
          "excerpt": "Former US President Bill Clinton is adding a new line to his résumé: novelist. The 42nd president is teaming up with..."
        },
        "editorial": {
          "subheading": "Former US leader co-writing ‘The President is Missing’ with author James Patterson",
          "byline": "Shannon Bond in New York and Courtney Weaver in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "1088a348-3008-11e7-9555-23ef563ecf9a",
        "apiUrl": "https://api.ft.com/content/1088a348-3008-11e7-9555-23ef563ecf9a",
        "title": {
          "title": "Comey defends handling of Trump and Clinton investigations"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-05-03T17:03:18Z",
          "lastPublishDateTime": "2017-05-03T19:52:48Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/1088a348-3008-11e7-9555-23ef563ecf9a"
        },
        "summary": {
          "excerpt": "...handling of politically-sensitive investigations involving Donald Trump and Hillary Clinton. Before the Senate Judiciary..."
        },
        "editorial": {
          "subheading": "FBI director says idea he cost Democrats the election makes him ‘mildly nauseous’",
          "byline": "By David J Lynch in Washington"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "cd5c33ed-1984-3d7a-8c8f-31fd42621f4b",
        "apiUrl": "https://api.ft.com/content/cd5c33ed-1984-3d7a-8c8f-31fd42621f4b",
        "title": {
          "title": "FBI director defends divulging Clinton probe"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-05-03T15:20:13Z",
          "lastPublishDateTime": "2017-05-03T15:20:13Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/cd5c33ed-1984-3d7a-8c8f-31fd42621f4b"
        },
        "summary": {
          "excerpt": "...Trump beat Mrs Clinton — was grilled by members of a US Senate panel on Wednesday morning over the letter, in which he..."
        },
        "editorial": {
          "byline": "Jessica Dye"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "e63646c4-1962-11e7-a53d-df09f373be87",
        "apiUrl": "https://api.ft.com/content/e63646c4-1962-11e7-a53d-df09f373be87",
        "title": {
          "title": "Beware of the return of the Clinton dynasty"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-04-05T12:34:09Z",
          "lastPublishDateTime": "2017-04-05T12:34:09Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/e63646c4-1962-11e7-a53d-df09f373be87"
        },
        "summary": {
          "excerpt": "..., Hillary Clinton is tiptoeing back into the light. She insists she has no plans to run for the White House again. The same..."
        },
        "editorial": {
          "subheading": "A public revival would blunt Democratic attacks on the Trump presidency",
          "byline": "Edward Luce"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "6a5e61e6-dec8-11e6-9d7c-be108f1c1dce",
        "apiUrl": "https://api.ft.com/content/6a5e61e6-dec8-11e6-9d7c-be108f1c1dce",
        "title": {
          "title": "Bill Clinton wins presidential stock market race"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-01-20T10:48:02Z",
          "lastPublishDateTime": "2017-01-20T10:48:02Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/6a5e61e6-dec8-11e6-9d7c-be108f1c1dce"
        },
        "summary": {
          "excerpt": "..., three presidents in succession have served two complete four-year terms. Those three men — Bill Clinton, George W Bush..."
        },
        "editorial": {
          "subheading": "Judged by returns from the S&P 500, Obama is edged out by his democratic predecessor",
          "byline": "John Authers"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "65abe5ba-dea5-11e6-9d7c-be108f1c1dce",
        "apiUrl": "https://api.ft.com/content/65abe5ba-dea5-11e6-9d7c-be108f1c1dce",
        "title": {
          "title": "Authers’ Note: Clinton wins!"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-01-20T00:23:36Z",
          "lastPublishDateTime": "2017-01-20T00:23:36Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/65abe5ba-dea5-11e6-9d7c-be108f1c1dce"
        },
        "summary": {
          "excerpt": "...— Bill Clinton, George W Bush and Barack Obama — tend to polarise opinion in different ways. The country seems far less..."
        },
        "editorial": {
          "subheading": "For stocks, the Obama era almost matched Clinton’s stunningly successful presidency",
          "byline": "John Authers"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "f7dff042-d8fc-11e6-944b-e7eb37a6aa8e",
        "apiUrl": "https://api.ft.com/content/f7dff042-d8fc-11e6-944b-e7eb37a6aa8e",
        "title": {
          "title": "FBI chief faces inquiry into handling of Clinton email scandal"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-01-12T20:54:33Z",
          "lastPublishDateTime": "2017-01-12T20:54:33Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/f7dff042-d8fc-11e6-944b-e7eb37a6aa8e"
        },
        "summary": {
          "excerpt": ".... The IG’s effort will not second-guess the FBI’s decision to recommend that Hillary Clinton not be indicted for her use..."
        },
        "editorial": {
          "subheading": "Inspector general opens probe after accusations Comey improperly influenced election",
          "byline": "David J Lynch in Washington"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "1b7bde59-8950-356f-b6d8-0388ff2e55e0",
        "apiUrl": "https://api.ft.com/content/1b7bde59-8950-356f-b6d8-0388ff2e55e0",
        "title": {
          "title": "DOJ watchdog to probe FBI’s handling of Clinton emails"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-01-12T18:59:13Z",
          "lastPublishDateTime": "2017-01-12T18:59:13Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/1b7bde59-8950-356f-b6d8-0388ff2e55e0"
        },
        "summary": {
          "excerpt": "...recommend that Hillary Clinton not be indicted for her use of a private email server while secretary of state...."
        },
        "editorial": {
          "byline": "Jessica Dye"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "452e7f6d-1a39-35c2-ac45-4116fffe34e8",
        "apiUrl": "https://api.ft.com/content/452e7f6d-1a39-35c2-ac45-4116fffe34e8",
        "title": {
          "title": "Trump’s AG nominee said he would step aside on Clinton emails, foundation"
        },
        "lifecycle": {
          "initialPublishDateTime": "2017-01-10T16:13:00Z",
          "lastPublishDateTime": "2017-01-10T16:13:00Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/452e7f6d-1a39-35c2-ac45-4116fffe34e8"
        },
        "summary": {
          "excerpt": "...Mr Trump’s electoral challenger Mrs Clinton impartially and objectively, Mr Sessions said that he believed that the..."
        },
        "editorial": {
          "byline": "Jessica Dye"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "a0e87a3a-bc09-11e6-8b45-b8b81dd5d080",
        "apiUrl": "https://api.ft.com/content/a0e87a3a-bc09-11e6-8b45-b8b81dd5d080",
        "title": {
          "title": "Hillary Clinton: a fate worse than mere defeat"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-12-08T05:01:13Z",
          "lastPublishDateTime": "2016-12-08T05:01:13Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/a0e87a3a-bc09-11e6-8b45-b8b81dd5d080"
        },
        "summary": {
          "excerpt": "It is safe to say Hillary Clinton will not look back on this year as a fulfilling one. Almost a century after women..."
        },
        "editorial": {
          "subheading": "She will go down in history as the presidential candidate who lost to outsider Donald Trump",
          "byline": "Edward Luce"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "a5d143fc-b4bf-11e6-ba85-95d1533d9a62",
        "apiUrl": "https://api.ft.com/content/a5d143fc-b4bf-11e6-ba85-95d1533d9a62",
        "title": {
          "title": "Trump attacks Clinton for backing recount effort"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-27T18:09:45Z",
          "lastPublishDateTime": "2016-11-28T00:40:41Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/a5d143fc-b4bf-11e6-ba85-95d1533d9a62"
        },
        "summary": {
          "excerpt": "...” of Americans cast ballots illegally. Following a weekend decision by the Clinton campaign to participate in a recount in..."
        },
        "editorial": {
          "subheading": "President-elect makes unsupported claim that millions voted illegally in US election",
          "byline": "Shawn Donnan in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "1aaa9010-b403-11e6-ba85-95d1533d9a62",
        "apiUrl": "https://api.ft.com/content/1aaa9010-b403-11e6-ba85-95d1533d9a62",
        "title": {
          "title": "Clinton campaign will take part in Wisconsin recount"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-26T18:27:35Z",
          "lastPublishDateTime": "2016-11-26T18:27:35Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/1aaa9010-b403-11e6-ba85-95d1533d9a62"
        },
        "summary": {
          "excerpt": "...were initiated too. But he said he did not expect Mr Trump’s victory to be overturned. If Mrs Clinton had won the three..."
        },
        "editorial": {
          "subheading": "Process initiated by Green party candidate Jill Stein",
          "byline": "Barney Jopson in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "e727bd4e-b006-11e6-a37c-f4a01f1b0fa1",
        "apiUrl": "https://api.ft.com/content/e727bd4e-b006-11e6-a37c-f4a01f1b0fa1",
        "title": {
          "title": "Bill Clinton was still ahead with the popular vote"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-25T21:40:30Z",
          "lastPublishDateTime": "2016-11-25T21:40:30Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/e727bd4e-b006-11e6-a37c-f4a01f1b0fa1"
        },
        "summary": {
          "excerpt": "Sir, While it is valid to point out that Bill Clinton did not secure 50 per cent of the popular vote in either of his..."
        },
        "editorial": {
          "subheading": "From Phil Thompson, London, UK"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "0ca350d4-ab65-11e6-9cb3-bb8207902122",
        "apiUrl": "https://api.ft.com/content/0ca350d4-ab65-11e6-9cb3-bb8207902122",
        "title": {
          "title": "Bill Clinton never got 50 per cent of the votes"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-18T21:58:23Z",
          "lastPublishDateTime": "2016-11-18T21:58:23Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/0ca350d4-ab65-11e6-9cb3-bb8207902122"
        },
        "summary": {
          "excerpt": "Sir, Lorenz Flückiger, writing from Oberwil/Zug, Switzerland ( Letters, November 14), points out that Hillary Clinton..."
        },
        "editorial": {
          "subheading": "From Louis Fourie, Ocean Ridge, FL, US"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "2c1be25c-a7f8-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/2c1be25c-a7f8-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "Trump v Clinton: Why the pollsters seemed to get it wrong"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-12T06:02:24Z",
          "lastPublishDateTime": "2016-11-12T06:02:24Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/2c1be25c-a7f8-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "...popular diviner of polling data, wrote on Wednesday: “If Clinton had done just 2 points better, pollsters would..."
        },
        "editorial": {
          "subheading": "Making election predictions was easier before big data, when everyone had a landline",
          "byline": "Philip Delves Broughton"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "ef21b3c0-a80d-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/ef21b3c0-a80d-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "Democrats see silver lining in release from Clinton grip"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-11T18:31:56Z",
          "lastPublishDateTime": "2016-11-11T18:31:56Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/ef21b3c0-a80d-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "..., the Clinton family crashed to earth on Wednesday, shattering the ambitions of Hillary Clinton and her..."
        },
        "editorial": {
          "subheading": "Party combs its ranks for progressive candidates to take over the helm",
          "byline": "Courtney Weaver and Barney Jopson in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "b9b73a0c-a6a0-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/b9b73a0c-a6a0-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "Emotional Clinton sees her dream of returning to the White House disappear"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-09T18:46:49Z",
          "lastPublishDateTime": "2016-11-09T18:46:49Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/b9b73a0c-a6a0-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "An emotional Hillary Clinton said Donald Trump deserved “the chance to lead” after his election victory, but..."
        },
        "editorial": {
          "subheading": "Candidate struggles to come to terms with second devastating defeat of her career",
          "byline": "Geoff Dyer in Washington and Courtney Weaver in New York"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "9613fe72-a69c-11e6-8b69-02899e8bd9d1",
        "apiUrl": "https://api.ft.com/content/9613fe72-a69c-11e6-8b69-02899e8bd9d1",
        "title": {
          "title": "Republicans keep door ajar to ‘locking up’ Clinton"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-09T18:17:13Z",
          "lastPublishDateTime": "2016-11-09T18:17:13Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/9613fe72-a69c-11e6-8b69-02899e8bd9d1"
        },
        "summary": {
          "excerpt": "...Hillary Clinton. Kellyanne Conway said Mr Trump and running mate Mike Pence had not discussed the issue in recent days. A..."
        },
        "editorial": {
          "subheading": "Potential prosecution is not open-and-shut case suggested on campaign trail",
          "byline": "David J Lynch in Washington"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "4e739087-d144-3813-90d7-383ddee550ec",
        "apiUrl": "https://api.ft.com/content/4e739087-d144-3813-90d7-383ddee550ec",
        "title": {
          "title": "Clinton concedes defeat after stunning election upset"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-09T17:00:55Z",
          "lastPublishDateTime": "2016-11-09T17:00:55Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/4e739087-d144-3813-90d7-383ddee550ec"
        },
        "summary": {
          "excerpt": "Hillary Clinton has spoken: It’s over. The Democratic presidential candidate struck an optimistic note in an emotional..."
        },
        "editorial": {
          "byline": "Jessica Dye"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "e7e59860-ae16-3a8b-bc4c-c97381d48060",
        "apiUrl": "https://api.ft.com/content/e7e59860-ae16-3a8b-bc4c-c97381d48060",
        "title": {
          "title": "Hillary Clinton concedes"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-09T17:00:10Z",
          "lastPublishDateTime": "2016-11-09T17:00:10Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/e7e59860-ae16-3a8b-bc4c-c97381d48060"
        },
        "summary": {
          "excerpt": "Former Secretary of State Hillary Clinton concedes the presidential election at the New Yorker Hotel in New York City..."
        },
        "editorial": {
          "byline": "Annabel Cook"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "24476d27-caf3-31a6-9e34-1fee5c8c8450",
        "apiUrl": "https://api.ft.com/content/24476d27-caf3-31a6-9e34-1fee5c8c8450",
        "title": {
          "title": "Clinton to speak at 9:30 am in New York"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-09T13:07:09Z",
          "lastPublishDateTime": "2016-11-09T13:07:09Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/24476d27-caf3-31a6-9e34-1fee5c8c8450"
        },
        "summary": {
          "excerpt": "Following what has surely been a long night, Hillary Clinton will break her silence in about 90 minutes. The Democratic..."
        },
        "editorial": {
          "byline": "Jessica Dye"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "ee515e45-39d8-3fb7-a294-9362d5de5828",
        "apiUrl": "https://api.ft.com/content/ee515e45-39d8-3fb7-a294-9362d5de5828",
        "title": {
          "title": "Hillary Clinton supporters in New York"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-09T08:00:00Z",
          "lastPublishDateTime": "2016-11-09T08:00:00Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/ee515e45-39d8-3fb7-a294-9362d5de5828"
        },
        "summary": {
          "excerpt": "Supporters of Democratic presidential nominee Hillary Clinton react to television reports during election night at the..."
        },
        "editorial": {
          "byline": "Helen Healy"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "91ff145a-b8ae-3b0f-9cdc-4c67129767fe",
        "apiUrl": "https://api.ft.com/content/91ff145a-b8ae-3b0f-9cdc-4c67129767fe",
        "title": {
          "title": "Clinton ‘not done yet’, campaign chair says"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-09T07:15:55Z",
          "lastPublishDateTime": "2016-11-09T07:15:55Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/91ff145a-b8ae-3b0f-9cdc-4c67129767fe"
        },
        "summary": {
          "excerpt": "Hillary Clinton is not ready to concede the election even as her odds of clinching a victory have dropped after she..."
        },
        "editorial": {
          "byline": "Adam Samson"
        }
      },
      {
        "aspectSet": "video",
        "modelVersion": "1",
        "id": "1607ee82-6109-3dd3-b320-0df8c7ae2a43",
        "apiUrl": "https://api.ft.com/content/1607ee82-6109-3dd3-b320-0df8c7ae2a43",
        "title": {
          "title": "Clinton and Trump supporters await results"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-09T04:06:24Z",
          "lastPublishDateTime": "2016-11-09T04:06:24Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/1607ee82-6109-3dd3-b320-0df8c7ae2a43"
        },
        "editorial": {
          "subheading": "Guest of both candidates watch results roll in",
          "byline": "Filmed by Ben Marino and Gregory Bobillot. Edited by Paolo Pascual."
        },
        "summary": {
          "excerpt": "FT reporters wait with supporters of both candidates for election results to roll in..."
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "259e0454-06e9-304a-9b94-c19199374bb1",
        "apiUrl": "https://api.ft.com/content/259e0454-06e9-304a-9b94-c19199374bb1",
        "title": {
          "title": "Trump projected to win Florida, Ohio; Clinton to nab Virginia"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-09T04:00:50Z",
          "lastPublishDateTime": "2016-11-09T04:00:50Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/259e0454-06e9-304a-9b94-c19199374bb1"
        },
        "summary": {
          "excerpt": "...Republican president George Bush in 2004. The AP also called Ohio for Mr Trump. Meanwhile, Hillary Clinton was..."
        },
        "editorial": {
          "byline": "Mamta Badkar"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "425ac29c-6392-30ae-bea4-8d827fd66c29",
        "apiUrl": "https://api.ft.com/content/425ac29c-6392-30ae-bea4-8d827fd66c29",
        "title": {
          "title": "Betting odds favour Clinton more as first results start coming in"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-08T23:33:52Z",
          "lastPublishDateTime": "2016-11-08T23:33:52Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/425ac29c-6392-30ae-bea4-8d827fd66c29"
        },
        "summary": {
          "excerpt": "...increasingly volatile as more results are reported. As The Upshot’s Nate Cohn has noted, Clinton is likely to lag in..."
        },
        "editorial": {
          "byline": "Hudson Lockett"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "abfa4372-a5d9-11e6-8b69-02899e8bd9d1",
        "apiUrl": "https://api.ft.com/content/abfa4372-a5d9-11e6-8b69-02899e8bd9d1",
        "title": {
          "title": "US stocks make ground as data start-up points to Clinton edge"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-08T19:36:56Z",
          "lastPublishDateTime": "2016-11-08T22:41:26Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/abfa4372-a5d9-11e6-8b69-02899e8bd9d1"
        },
        "summary": {
          "excerpt": "...nominee Hillary Clinton an edge in key swing states and pointed to her election by the end of the night. Some..."
        },
        "editorial": {
          "subheading": "VoteCastr figures start to swing sentiment as it gives Democrat lead in key states",
          "byline": "Eric Platt and Nicole Bullock in New York"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "65a1ad11-6151-332c-abb8-da0e9e6e67f0",
        "apiUrl": "https://api.ft.com/content/65a1ad11-6151-332c-abb8-da0e9e6e67f0",
        "title": {
          "title": "Yen losses pile up as Clinton edge spurs risk-on rally"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-08T19:11:30Z",
          "lastPublishDateTime": "2016-11-08T19:11:30Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/65a1ad11-6151-332c-abb8-da0e9e6e67f0"
        },
        "summary": {
          "excerpt": "...Tuesday as investors increased their risk appetite amid early signs that Democratic nominee Hillary Clinton has an edge..."
        },
        "editorial": {
          "byline": "Wataru Suzuki"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "29f69058-bab7-3e41-96b4-35d7c3eae0a2",
        "apiUrl": "https://api.ft.com/content/29f69058-bab7-3e41-96b4-35d7c3eae0a2",
        "title": {
          "title": "Wall St extends rally on signs of Clinton early edge"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-08T18:13:17Z",
          "lastPublishDateTime": "2016-11-08T18:13:17Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/29f69058-bab7-3e41-96b4-35d7c3eae0a2"
        },
        "summary": {
          "excerpt": "Tell us markets, how do you REALLY feel about a Clinton presidency? Wall Street extended its gains on Tuesday, with the..."
        },
        "editorial": {
          "byline": "Jessica Dye"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "77060aa2-a4f8-11e6-8b69-02899e8bd9d1",
        "apiUrl": "https://api.ft.com/content/77060aa2-a4f8-11e6-8b69-02899e8bd9d1",
        "title": {
          "title": "US election 2016: how Trump and Clinton fought for White House"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-08T05:43:56Z",
          "lastPublishDateTime": "2016-11-08T14:52:11Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/77060aa2-a4f8-11e6-8b69-02899e8bd9d1"
        },
        "summary": {
          "excerpt": "...election campaigns in recent history.   Hillary Clinton unofficially began her campaign for president in April last year..."
        },
        "editorial": {
          "subheading": "Words, quotes and tweets that defined presidential race",
          "byline": "Geoff Dyer in Washington"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "4862df08-cea0-324a-8270-80bec0c581ac",
        "apiUrl": "https://api.ft.com/content/4862df08-cea0-324a-8270-80bec0c581ac",
        "title": {
          "title": "Praying for Hillary Clinton in India"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-08T11:30:26Z",
          "lastPublishDateTime": "2016-11-08T11:30:26Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/4862df08-cea0-324a-8270-80bec0c581ac"
        },
        "summary": {
          "excerpt": "Indian devotees perform a prayer with a photograph of US Presidential candidate Hillary Clinton at a temple in Varanasi..."
        },
        "editorial": {
          "byline": "Helen Healy"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "1b9d6224-a515-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/1b9d6224-a515-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "Clinton and Trump in final swing-state blitz"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T19:44:14Z",
          "lastPublishDateTime": "2016-11-08T11:22:16Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/1b9d6224-a515-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "Hillary Clinton and Donald Trump made their closing arguments for the White House on Monday night, with the Democrat..."
        },
        "editorial": {
          "subheading": "Democrat narrowly wins traditional first tally in New Hampshire’s tiny Dixville Notch",
          "byline": "Sam Fleming in Philadelphia, Courtney Weaver in Manchester, and Barney Jopson and Demetri Sevastopulo in Washington"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "c1aa46b8-24c0-3215-a9ce-9bce391659e1",
        "apiUrl": "https://api.ft.com/content/c1aa46b8-24c0-3215-a9ce-9bce391659e1",
        "title": {
          "title": "New Hampshire’s Hart’s Location declares for Clinton"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-08T08:21:56Z",
          "lastPublishDateTime": "2016-11-08T08:21:56Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/c1aa46b8-24c0-3215-a9ce-9bce391659e1"
        },
        "summary": {
          "excerpt": "...weight behind Hillary Clinton, though by a narrow margin. Mark Dindorf, head of local government in the New Hampshire..."
        },
        "editorial": {
          "byline": "Katie Martin"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "cfe30095-0fb3-3868-9bf6-bd6a55c0f8ae",
        "apiUrl": "https://api.ft.com/content/cfe30095-0fb3-3868-9bf6-bd6a55c0f8ae",
        "title": {
          "title": "Clinton in the lead as polls open (and close) in Dixville Notch"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-08T06:39:05Z",
          "lastPublishDateTime": "2016-11-08T06:39:05Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/cfe30095-0fb3-3868-9bf6-bd6a55c0f8ae"
        },
        "summary": {
          "excerpt": "And we’re off. Hillary Clinton has her nose in front as one of the first places in the US delivered its judgement on..."
        },
        "editorial": {
          "byline": "Peter Wells"
        }
      },
      {
        "aspectSet": "video",
        "modelVersion": "1",
        "id": "f4d8ca21-a898-397c-8699-ff0b4dc0ab90",
        "apiUrl": "https://api.ft.com/content/f4d8ca21-a898-397c-8699-ff0b4dc0ab90",
        "title": {
          "title": "Trump and Clinton policies"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-08T06:07:22Z",
          "lastPublishDateTime": "2016-11-08T06:07:22Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/f4d8ca21-a898-397c-8699-ff0b4dc0ab90"
        },
        "editorial": {
          "subheading": "White House hopefuls’ plans for office ",
          "byline": "Produced by Josh de la Mare. Edited by Gregory Bobillot and Felline Reyes. Library: Getty"
        },
        "summary": {
          "excerpt": "There has been little focus on the policies of US presidential candidates Donald Trump and Hillary Clinton. What..."
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "59791078-a512-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/59791078-a512-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "The Hillary Clinton hate campaign has twisted America"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-08T05:00:31Z",
          "lastPublishDateTime": "2016-11-08T05:00:31Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/59791078-a512-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "...Hillary Clinton is what she is, or appears to be, today because America has been taught to hate her for a quarter of a..."
        },
        "editorial": {
          "subheading": "Much of the blame for the lack of civil political discourse is down to Newt Gingrich",
          "byline": "Jurek Martin"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "fb8ad540-a495-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/fb8ad540-a495-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "Risk appetite buoyed by Clinton victory hopes"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T03:35:57Z",
          "lastPublishDateTime": "2016-11-07T21:04:09Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/fb8ad540-a495-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "...palpable sense of relief descended over markets after the Federal Bureau of Investigation once again cleared Hillary..."
        },
        "editorial": {
          "subheading": "S&P 500 has best day since March while gold and yen slide",
          "byline": "Dave Shellock"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "2899f4ac-ce0e-3bbf-ac37-4c1082e9cf92",
        "apiUrl": "https://api.ft.com/content/2899f4ac-ce0e-3bbf-ac37-4c1082e9cf92",
        "title": {
          "title": "Yen skids as odds of Clinton win climb"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T17:00:05Z",
          "lastPublishDateTime": "2016-11-07T17:00:05Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/2899f4ac-ce0e-3bbf-ac37-4c1082e9cf92"
        },
        "summary": {
          "excerpt": "...revived bets for a Hillary Clinton Presidency after the FBI said it would take no additional action regarding her emails..."
        },
        "editorial": {
          "byline": "Wataru Suzuki"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "02618123-c02f-341d-9b82-6857e3bb79c8",
        "apiUrl": "https://api.ft.com/content/02618123-c02f-341d-9b82-6857e3bb79c8",
        "title": {
          "title": "Mexican stocks and ETFs join peso in Clinton relief rally"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T16:30:14Z",
          "lastPublishDateTime": "2016-11-07T16:30:14Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/02618123-c02f-341d-9b82-6857e3bb79c8"
        },
        "summary": {
          "excerpt": "Who will win tomorrow’s US election? According to Mexico’s peso and stocks, Hillary Clinton is home and dry. The peso..."
        },
        "editorial": {
          "byline": "Jude Webber"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "26fead50-a4be-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/26fead50-a4be-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "Wall St rallies as investors bet on Clinton win"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T09:24:36Z",
          "lastPublishDateTime": "2016-11-07T15:50:04Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/26fead50-a4be-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "...Clinton and Mr Trump were on Monday beginning their final dashes across the country to rally swing state..."
        },
        "editorial": {
          "subheading": "FBI letter to take no action against Democrat helps candidate clear big hurdle",
          "byline": "FT reporters"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "fb3a262f-1f02-3a02-afd8-0b555335699c",
        "apiUrl": "https://api.ft.com/content/fb3a262f-1f02-3a02-afd8-0b555335699c",
        "title": {
          "title": "Odds of Clinton win jump on prediction markets"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T15:26:14Z",
          "lastPublishDateTime": "2016-11-07T15:26:14Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/fb3a262f-1f02-3a02-afd8-0b555335699c"
        },
        "summary": {
          "excerpt": "...email server. The implied probability of Mrs Clinton clinching the vote climbed to 89 per cent on Monday, from as low..."
        },
        "editorial": {
          "byline": "Adam Samson"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "47f16aa1-71e8-3308-af9d-21454d987a8b",
        "apiUrl": "https://api.ft.com/content/47f16aa1-71e8-3308-af9d-21454d987a8b",
        "title": {
          "title": "New polls give Clinton slim lead over Trump"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T14:48:30Z",
          "lastPublishDateTime": "2016-11-07T14:48:30Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/47f16aa1-71e8-3308-af9d-21454d987a8b"
        },
        "summary": {
          "excerpt": "...that Democratic nominee Hillary Clinton holding a slim lead over Republican rival Donald Trump. Two tracking..."
        },
        "editorial": {
          "byline": "Pan Kwan Yuk"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "503ff7d8-a47c-11e6-8b69-02899e8bd9d1",
        "apiUrl": "https://api.ft.com/content/503ff7d8-a47c-11e6-8b69-02899e8bd9d1",
        "title": {
          "title": "Trump and Clinton: key policies"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T14:41:20Z",
          "lastPublishDateTime": "2016-11-07T14:41:20Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/503ff7d8-a47c-11e6-8b69-02899e8bd9d1"
        },
        "summary": {
          "excerpt": "...and turning his back on international trade pacts. Mrs Clinton is more wary of free trade than she once was, but her..."
        },
        "editorial": {
          "subheading": "Republican’s ideas defy orthodoxy while Democratic rival aims to unite party factions",
          "byline": "Barney Jopson in Washington"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "87693418-639d-30e6-991f-6b33ab1e3549",
        "apiUrl": "https://api.ft.com/content/87693418-639d-30e6-991f-6b33ab1e3549",
        "title": {
          "title": "Wall Street set for rally as FBI absolves Clinton"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T13:00:49Z",
          "lastPublishDateTime": "2016-11-07T13:00:49Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/87693418-639d-30e6-991f-6b33ab1e3549"
        },
        "summary": {
          "excerpt": "...that it would take no action against Hillary Clinton over her emails, removing a cloud over the Democratic candidate ahead..."
        },
        "editorial": {
          "byline": "Pan Kwan Yuk"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "41f9b0fa-a43c-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/41f9b0fa-a43c-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "Clinton has been swiftboated by Trump and his acolytes"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T11:45:32Z",
          "lastPublishDateTime": "2016-11-07T11:45:32Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/41f9b0fa-a43c-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "...Clinton go from being rated as the most admired woman that Americans “have heard about or read about, living in..."
        },
        "editorial": {
          "subheading": "Even-handed media coverage has created a false equivalence",
          "byline": "Gloria Steinem"
        }
      },
      {
        "aspectSet": "video",
        "modelVersion": "1",
        "id": "4e209aa4-4086-37c4-9d58-08a25e671f0d",
        "apiUrl": "https://api.ft.com/content/4e209aa4-4086-37c4-9d58-08a25e671f0d",
        "title": {
          "title": "FBI clears Clinton, HK legislators"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T10:26:38Z",
          "lastPublishDateTime": "2016-11-07T10:26:38Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/4e209aa4-4086-37c4-9d58-08a25e671f0d"
        },
        "editorial": {
          "subheading": "The FT provides Monday’s top stories",
          "byline": "Produced and filmed by Petros Gioumpasis."
        },
        "summary": {
          "excerpt": "...candidate Hillary Clinton over new emails, China moving to block Hong Kong lawmakers and Europe’s ‘happiness..."
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "61b9a6f3-ea35-318c-9175-db3b5320346e",
        "apiUrl": "https://api.ft.com/content/61b9a6f3-ea35-318c-9175-db3b5320346e",
        "title": {
          "title": "Dollar ‘doubling down’ on Clinton"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T09:16:31Z",
          "lastPublishDateTime": "2016-11-07T09:16:31Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/61b9a6f3-ea35-318c-9175-db3b5320346e"
        },
        "summary": {
          "excerpt": "...twist in the US presidential election campaign. News that the FBI will take no action against Hillary Clinton after its..."
        },
        "editorial": {
          "byline": "Katie Martin"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "eea107e0-a46e-11e6-8b69-02899e8bd9d1",
        "apiUrl": "https://api.ft.com/content/eea107e0-a46e-11e6-8b69-02899e8bd9d1",
        "title": {
          "title": "FBI to take no action against Clinton over new emails"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-06T22:56:26Z",
          "lastPublishDateTime": "2016-11-07T08:33:07Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/eea107e0-a46e-11e6-8b69-02899e8bd9d1"
        },
        "summary": {
          "excerpt": "...changed our conclusions that we expressed in July with respect to Secretary Clinton,” James Comey wrote to lawmakers on..."
        },
        "editorial": {
          "subheading": "Agency chief stands by July decision as candidates embark on last days of campaigning",
          "byline": "Demetri Sevastopulo and Geoff Dyer in Washington"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "240bd48d-f175-3ef3-8bd4-522e7f7e1c1c",
        "apiUrl": "https://api.ft.com/content/240bd48d-f175-3ef3-8bd4-522e7f7e1c1c",
        "title": {
          "title": "Clinton reprieve bumps up global markets"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T07:49:05Z",
          "lastPublishDateTime": "2016-11-07T07:49:05Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/240bd48d-f175-3ef3-8bd4-522e7f7e1c1c"
        },
        "summary": {
          "excerpt": "...Hillary Clinton over her emails is seen boosting the chances of the Democratic candidate being elected..."
        },
        "editorial": {
          "byline": "Jamie Chisholm"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "90f15c5d-2f97-3938-b4ab-3282ef612cfa",
        "apiUrl": "https://api.ft.com/content/90f15c5d-2f97-3938-b4ab-3282ef612cfa",
        "title": {
          "title": "Fast Europe Open: Relief rally as FBI backs down on Clinton emails"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T06:03:01Z",
          "lastPublishDateTime": "2016-11-07T06:03:01Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/90f15c5d-2f97-3938-b4ab-3282ef612cfa"
        },
        "summary": {
          "excerpt": "...Mrs Clinton following its investigation of new emails related to the probe of the presidential candidate, spurring..."
        },
        "editorial": {
          "byline": "Peter Wells"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "788d6d14-1293-34f2-ab03-b91cbd948bbc",
        "apiUrl": "https://api.ft.com/content/788d6d14-1293-34f2-ab03-b91cbd948bbc",
        "title": {
          "title": "Markets recover after FBI backtracks on Clinton emails"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T03:19:27Z",
          "lastPublishDateTime": "2016-11-07T03:19:27Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/788d6d14-1293-34f2-ab03-b91cbd948bbc"
        },
        "summary": {
          "excerpt": "...against Mrs Clinton regarding its probe into new emails related to the probe of the presidential candidate. Markets were..."
        },
        "editorial": {
          "byline": "Peter Wells"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "ad499500-fc0d-3ec5-b4f9-9609fd66388b",
        "apiUrl": "https://api.ft.com/content/ad499500-fc0d-3ec5-b4f9-9609fd66388b",
        "title": {
          "title": "Peso, dollar gain as FBI backtracks on Clinton emails"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-07T01:09:08Z",
          "lastPublishDateTime": "2016-11-07T01:09:08Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/ad499500-fc0d-3ec5-b4f9-9609fd66388b"
        },
        "summary": {
          "excerpt": "...action against Hillary Clinton following its examination of new emails related to the presidential nominee’s..."
        },
        "editorial": {
          "byline": "Peter Wells"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "5bc083f0-aab0-31aa-8413-d60973caf40d",
        "apiUrl": "https://api.ft.com/content/5bc083f0-aab0-31aa-8413-d60973caf40d",
        "title": {
          "title": "Fast Asia Open: Markets warm to FBI backtrack in Clinton email probe"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-06T23:36:22Z",
          "lastPublishDateTime": "2016-11-06T23:36:22Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/5bc083f0-aab0-31aa-8413-d60973caf40d"
        },
        "summary": {
          "excerpt": "...action against Hillary Clinton in relation to emails from her private server. The development comes just two days away..."
        },
        "editorial": {
          "byline": "Peter Wells"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "1d69272a-a446-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/1d69272a-a446-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "Trump and Clinton focus frantic final push on battleground states"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-06T19:15:54Z",
          "lastPublishDateTime": "2016-11-06T23:02:35Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/1d69272a-a446-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "Hillary Clinton and Donald Trump launched a two-day campaign whirlwind in battleground states on Sunday in a frantic..."
        },
        "editorial": {
          "subheading": "Surge in Latino voting boosts Democrat, while  Republican looks to white working class",
          "byline": "Geoff Dyer and Shawn Donnan in Washington and Patti Waldmeir in Lansing"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "d552f55e-a366-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/d552f55e-a366-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "Clinton and Trump make final battleground push amid tight race"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-05T14:57:06Z",
          "lastPublishDateTime": "2016-11-06T19:27:33Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/d552f55e-a366-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "Hillary Clinton and Donald Trump focused their campaign fire on Florida, North Carolina and other battleground states..."
        },
        "editorial": {
          "subheading": "Democrats will send President Obama to play defence in Michigan ",
          "byline": "Shawn Donnan in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "4ae53618-a274-11e6-82c3-4351ce86813f",
        "apiUrl": "https://api.ft.com/content/4ae53618-a274-11e6-82c3-4351ce86813f",
        "title": {
          "title": "Big question for markets this week: Clinton or Trump?"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-06T17:07:30Z",
          "lastPublishDateTime": "2016-11-06T17:07:30Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/4ae53618-a274-11e6-82c3-4351ce86813f"
        },
        "summary": {
          "excerpt": "...Clinton, but investors have been pulling money from equities and riskier bonds. Gold, the Japanese yen, the Swiss franc..."
        },
        "editorial": {
          "subheading": "US election will dominate coming week as investors prepare to deal with their next dose of political risk",
          "byline": "Michael Mackenzie, Elaine Moore and Michael Hunter"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "5b84058e-a432-11e6-8898-79a99e2a4de6",
        "apiUrl": "https://api.ft.com/content/5b84058e-a432-11e6-8898-79a99e2a4de6",
        "title": {
          "title": "Clinton moves to guard ‘firewall’ states and gain black vote"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-06T16:49:23Z",
          "lastPublishDateTime": "2016-11-06T16:49:23Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/5b84058e-a432-11e6-8898-79a99e2a4de6"
        },
        "summary": {
          "excerpt": "...arena where Mr Obama was passionately urging the mostly black attendees to vote for Hillary Clinton. “I need you not..."
        },
        "editorial": {
          "subheading": "Democratic candidate forced to guard ‘strongholds’ as Obama energises African-Americans",
          "byline": "Demetri Sevastopulo in Fayetteville, Barney Jopson in Detroit and Patti Waldmeir in Lansing"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "f56926d0-a21a-11e6-aa83-bcb58d1d2193",
        "apiUrl": "https://api.ft.com/content/f56926d0-a21a-11e6-aa83-bcb58d1d2193",
        "title": {
          "title": "Corporate US dismayed over choice of Clinton or Trump"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-06T12:05:42Z",
          "lastPublishDateTime": "2016-11-06T12:05:42Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/f56926d0-a21a-11e6-aa83-bcb58d1d2193"
        },
        "summary": {
          "excerpt": "US businesses are deeply dissatisfied with Hillary Clinton and Donald Trump for shunning the concerns of corporate..."
        },
        "editorial": {
          "subheading": "Survey of lobby groups representing 30,000 companies shows Democratic win preferred",
          "byline": "Barney Jopson in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "bf1da11a-a2b4-11e6-aa83-bcb58d1d2193",
        "apiUrl": "https://api.ft.com/content/bf1da11a-a2b4-11e6-aa83-bcb58d1d2193",
        "title": {
          "title": "Early voting leaves Clinton-Trump race on a knife edge"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-04T19:27:16Z",
          "lastPublishDateTime": "2016-11-04T22:41:45Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/bf1da11a-a2b4-11e6-aa83-bcb58d1d2193"
        },
        "summary": {
          "excerpt": "...Latinos, seen as a fillip to Hillary Clinton, and whites, where Donald Trump pulls most of his support, giving both..."
        },
        "editorial": {
          "subheading": "Both campaigns claim momentum in key states going into the election’s final weekend",
          "byline": "Geoff Dyer in Washington, Demetri Sevastopulo in Fayetteville, North Carolina, and Barney Jopson in Detroit, Michigan"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "e01abff0-5292-11e6-9664-e0bdc13c3bef",
        "apiUrl": "https://api.ft.com/content/e01abff0-5292-11e6-9664-e0bdc13c3bef",
        "title": {
          "title": "Trump vs Clinton: presidential election poll tracker"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-07-26T16:02:04Z",
          "lastPublishDateTime": "2016-11-04T15:56:33Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/e01abff0-5292-11e6-9664-e0bdc13c3bef"
        },
        "summary": {
          "excerpt": "This interactive content is best viewed in a web browser. Click here to open the story outside of the FT web app..."
        },
        "editorial": {
          "subheading": "Who is ahead in national and swing state polls?",
          "byline": "Joanna S Kao, Tom Pearson, Steve Bernard, Luke Kavanagh, Callum Locke, Claire Manibog, Ændrew Rininsland, Caroline Nevitt, Martin Stabe and Billy Ehrenberg-Shannon"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "43fa5d1c-6ab5-3b68-8547-082179455823",
        "apiUrl": "https://api.ft.com/content/43fa5d1c-6ab5-3b68-8547-082179455823",
        "title": {
          "title": "Charles Schwab survey shows investors prefer Clinton by wide margin"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-04T15:21:44Z",
          "lastPublishDateTime": "2016-11-04T15:21:44Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/43fa5d1c-6ab5-3b68-8547-082179455823"
        },
        "summary": {
          "excerpt": "...polls, a survey of Charles Schwab’s investors showed that nearly half planned to vote for Mrs Clinton, compared..."
        },
        "editorial": {
          "byline": "Mamta Badkar"
        }
      },
      {
        "aspectSet": "video",
        "modelVersion": "1",
        "id": "7bf54696-744b-392d-9643-6804821de323",
        "apiUrl": "https://api.ft.com/content/7bf54696-744b-392d-9643-6804821de323",
        "title": {
          "title": "Final days of Trump-Clinton race"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-04T11:03:00Z",
          "lastPublishDateTime": "2016-11-04T11:03:00Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/7bf54696-744b-392d-9643-6804821de323"
        },
        "editorial": {
          "subheading": "FBI letter closes gap between US candidates",
          "byline": "Produced and filmed by Gregory Bobillot. Images courtesy of NowThis, Reuters and Getty."
        },
        "summary": {
          "excerpt": "The FT's Neil Munshi rounds up the US election stories. Democrat Hillary Clinton’s lead over Republican rival Donald..."
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "fe6d30d2-a085-11e6-891e-abe238dee8e2",
        "apiUrl": "https://api.ft.com/content/fe6d30d2-a085-11e6-891e-abe238dee8e2",
        "title": {
          "title": "Will ‘foot-dragging’ trip Clinton up?"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-04T10:31:31Z",
          "lastPublishDateTime": "2016-11-04T10:31:31Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/fe6d30d2-a085-11e6-891e-abe238dee8e2"
        },
        "summary": {
          "excerpt": "...Clinton. No surprise there: Schoen, now a political analyst for Fox News, has been a friend of the Clintons since 1994..."
        },
        "editorial": {
          "subheading": "‘Faced with a world that seems disempowering, bizarre and hostile, some voters prefer to opt out rather than openly protest’",
          "byline": "Gillian Tett"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "67ff8d88-a1ee-11e6-82c3-4351ce86813f",
        "apiUrl": "https://api.ft.com/content/67ff8d88-a1ee-11e6-82c3-4351ce86813f",
        "title": {
          "title": "Trump takes aim at Clinton ‘firewall’ states as polls narrow"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-03T19:00:42Z",
          "lastPublishDateTime": "2016-11-04T01:17:02Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/67ff8d88-a1ee-11e6-82c3-4351ce86813f"
        },
        "summary": {
          "excerpt": "Hillary Clinton and Donald Trump scrambled for votes in North Carolina on Thursday as the final days of the..."
        },
        "editorial": {
          "subheading": "Both candidates focus on North Carolina as more states become battlegrounds",
          "byline": "Demetri Sevastopulo and Barney Jopson in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "54e5295c-a021-11e6-891e-abe238dee8e2",
        "apiUrl": "https://api.ft.com/content/54e5295c-a021-11e6-891e-abe238dee8e2",
        "title": {
          "title": "Clinton or Trump? It shouldn’t matter to investors"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-02T05:30:41Z",
          "lastPublishDateTime": "2016-11-02T05:30:41Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/54e5295c-a021-11e6-891e-abe238dee8e2"
        },
        "summary": {
          "excerpt": "Soon we’ll know who America’s next president is. There are no guarantees, but barring something wild, Hillary Clinton..."
        },
        "editorial": {
          "subheading": "Whoever wins will be stymied by Congress",
          "byline": "Ken Fisher"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "738c4fea-a088-11e6-86d5-4e36b35c3550",
        "apiUrl": "https://api.ft.com/content/738c4fea-a088-11e6-86d5-4e36b35c3550",
        "title": {
          "title": "Clinton campaign renews attack on Trump’s treatment of women"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-01T23:48:49Z",
          "lastPublishDateTime": "2016-11-01T23:48:49Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/738c4fea-a088-11e6-86d5-4e36b35c3550"
        },
        "summary": {
          "excerpt": "...Clinton spent most of October laying out a more positive case for her candidacy — and moving beyond her anti-Trump..."
        },
        "editorial": {
          "subheading": "Democratic candidate tries to regain momentum in narrowing US election race",
          "byline": "Courtney Weaver and David J Lynch in Washington"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "92be7378-f1ac-3bac-a2da-3fca69304cc6",
        "apiUrl": "https://api.ft.com/content/92be7378-f1ac-3bac-a2da-3fca69304cc6",
        "title": {
          "title": "Hillary Clinton in masquerade"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-11-01T11:00:41Z",
          "lastPublishDateTime": "2016-11-01T11:00:41Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/92be7378-f1ac-3bac-a2da-3fca69304cc6"
        },
        "summary": {
          "excerpt": "Democratic presidential nominee Hillary Clinton holds a masquerade mask as she jokes with members of her staff aboard..."
        },
        "editorial": {
          "byline": "Helen Healy"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "215e5080-9f50-11e6-86d5-4e36b35c3550",
        "apiUrl": "https://api.ft.com/content/215e5080-9f50-11e6-86d5-4e36b35c3550",
        "title": {
          "title": "The Clinton email controversy: what we know"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-31T15:24:36Z",
          "lastPublishDateTime": "2016-10-31T15:24:36Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/215e5080-9f50-11e6-86d5-4e36b35c3550"
        },
        "summary": {
          "excerpt": ".... It concluded “no reasonable prosecutor” would bring a criminal case against Mrs Clinton, but she and her aides had been..."
        },
        "editorial": {
          "subheading": "FT explainer on the debate over the FBI’s intervention",
          "byline": "John Murray Brown"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "f61b93c8-9f5a-11e6-891e-abe238dee8e2",
        "apiUrl": "https://api.ft.com/content/f61b93c8-9f5a-11e6-891e-abe238dee8e2",
        "title": {
          "title": "FT endorsement: For all her weaknesses, Clinton is the best hope"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-31T12:18:18Z",
          "lastPublishDateTime": "2016-10-31T12:18:18Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/f61b93c8-9f5a-11e6-891e-abe238dee8e2"
        },
        "summary": {
          "excerpt": "...Clinton and Donald Trump has provided high drama, amply demonstrated by the Federal Bureau of Investigation’s reckless..."
        },
        "editorial": {
          "subheading": "She is manifestly more competent than Trump with his braggadocio and divisiveness"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "19a3c8e8-9ebf-11e6-891e-abe238dee8e2",
        "apiUrl": "https://api.ft.com/content/19a3c8e8-9ebf-11e6-891e-abe238dee8e2",
        "title": {
          "title": "FBI chief may have violated law in Clinton probe, Democrat claims"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-30T19:27:15Z",
          "lastPublishDateTime": "2016-10-31T11:48:39Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/19a3c8e8-9ebf-11e6-891e-abe238dee8e2"
        },
        "summary": {
          "excerpt": "...every opportunity”. The allegations from a top Democratic officeholder with close ties to Mrs Clinton were the clearest..."
        },
        "editorial": {
          "subheading": "Harry Reid claims James Comey is sitting on ‘explosive’ evidence about Trump and Russia",
          "byline": "David J Lynch in Washington"
        }
      },
      {
        "aspectSet": "video",
        "modelVersion": "1",
        "id": "4f324f03-f712-3724-87e5-f83935234b75",
        "apiUrl": "https://api.ft.com/content/4f324f03-f712-3724-87e5-f83935234b75",
        "title": {
          "title": "Clinton, Trump and the yen"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-31T10:44:05Z",
          "lastPublishDateTime": "2016-10-31T10:44:05Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/4f324f03-f712-3724-87e5-f83935234b75"
        },
        "editorial": {
          "subheading": "Dollar-yen rate and the US election",
          "byline": "Edited by Paolo Pascual."
        },
        "summary": {
          "excerpt": "The FT's Leo Lewis on why Japan's life insurers desperately want the US election to be over..."
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "11b195e6-9def-11e6-891e-abe238dee8e2",
        "apiUrl": "https://api.ft.com/content/11b195e6-9def-11e6-891e-abe238dee8e2",
        "title": {
          "title": "Hillary Clinton campaign berates FBI over email probe"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-29T16:37:44Z",
          "lastPublishDateTime": "2016-10-30T15:31:05Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/11b195e6-9def-11e6-891e-abe238dee8e2"
        },
        "summary": {
          "excerpt": "The Clinton campaign lashed out at the director of the FBI over the weekend after news that it was investigating a..."
        },
        "editorial": {
          "subheading": "Announcement of investigation made as polls tighten in final days of campaign",
          "byline": "By David J Lynch and Barney Jopson in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "44876fa4-9e26-11e6-891e-abe238dee8e2",
        "apiUrl": "https://api.ft.com/content/44876fa4-9e26-11e6-891e-abe238dee8e2",
        "title": {
          "title": "FBI chief James Comey under fire for Clinton email investigation"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-29T23:07:02Z",
          "lastPublishDateTime": "2016-10-30T14:11:39Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/44876fa4-9e26-11e6-891e-abe238dee8e2"
        },
        "summary": {
          "excerpt": "Hillary Clinton is not the only one in trouble over her emails. James Comey, the FBI director, is also under intense..."
        },
        "editorial": {
          "subheading": "Director criticised for case review from both sides of the political aisle",
          "byline": "David J Lynch in Washington"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "7e71f0f2-9d3d-11e6-8324-be63473ce146",
        "apiUrl": "https://api.ft.com/content/7e71f0f2-9d3d-11e6-8324-be63473ce146",
        "title": {
          "title": "Tremor over Clinton emails gives glimpse of market view of Trump"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T19:41:19Z",
          "lastPublishDateTime": "2016-10-28T23:15:12Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/7e71f0f2-9d3d-11e6-8324-be63473ce146"
        },
        "summary": {
          "excerpt": "...presidential candidates. While the Trump index was down 0.2 per cent on Friday, the Clinton gauge fell 1.9 per..."
        },
        "editorial": {
          "subheading": "US stock market dips and Wall Street ‘Fear Index’ hits one-month high",
          "byline": "Robin Wigglesworth, Adam Samson and Mamta Badkar in New York"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "a364f244-9d4f-11e6-a6e4-8b8e77dd083a",
        "apiUrl": "https://api.ft.com/content/a364f244-9d4f-11e6-a6e4-8b8e77dd083a",
        "title": {
          "title": "FBI drops bizarre October surprise on Clinton"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T21:24:36Z",
          "lastPublishDateTime": "2016-10-28T21:24:36Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/a364f244-9d4f-11e6-a6e4-8b8e77dd083a"
        },
        "summary": {
          "excerpt": "...the FBI to say it is looking at fresh evidence in the Clinton email investigation. It is quite another if it came from a..."
        },
        "editorial": {
          "subheading": "Whatever its findings, the probe has already damaged the Democrat, writes Edward Luce",
          "byline": "Edward Luce"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "2cd98c72-d9eb-3aab-a964-9b372d2fffca",
        "apiUrl": "https://api.ft.com/content/2cd98c72-d9eb-3aab-a964-9b372d2fffca",
        "title": {
          "title": "US stocks slip as investors eye Clinton probe"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T20:46:50Z",
          "lastPublishDateTime": "2016-10-28T20:46:50Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/2cd98c72-d9eb-3aab-a964-9b372d2fffca"
        },
        "summary": {
          "excerpt": "...investigation into emails involving US presidential candidate Hillary Clinton. The S&P 500 reversed earlier..."
        },
        "editorial": {
          "byline": "Wataru Suzuki"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "4ee29364-b7c5-3fcd-b888-d7f769032196",
        "apiUrl": "https://api.ft.com/content/4ee29364-b7c5-3fcd-b888-d7f769032196",
        "title": {
          "title": "Clinton campaign demands more details of FBI investigation"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T19:51:37Z",
          "lastPublishDateTime": "2016-10-28T19:51:37Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/4ee29364-b7c5-3fcd-b888-d7f769032196"
        },
        "summary": {
          "excerpt": "Democratic presidential candidate Hillary Clinton’s campaign chairman John Podesta has just issued a statement in..."
        },
        "editorial": {
          "byline": "Pan Kwan Yuk"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "833a6e0f-5fa6-3075-affb-06d583326e00",
        "apiUrl": "https://api.ft.com/content/833a6e0f-5fa6-3075-affb-06d583326e00",
        "title": {
          "title": "Clinton stays mum on new email revelations at campaign rally"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T19:34:15Z",
          "lastPublishDateTime": "2016-10-28T19:34:15Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/833a6e0f-5fa6-3075-affb-06d583326e00"
        },
        "summary": {
          "excerpt": "If Democratic presidential nominee Hillary Clinton has any reaction to the news that the FBI has launched a new..."
        },
        "editorial": {
          "byline": "Jessica Dye"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "d76b5baf-051d-30ae-8fb8-47c60b8ea2fa",
        "apiUrl": "https://api.ft.com/content/d76b5baf-051d-30ae-8fb8-47c60b8ea2fa",
        "title": {
          "title": "Odds of Clinton win slip in prediction markets"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T19:29:50Z",
          "lastPublishDateTime": "2016-10-28T19:29:50Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/d76b5baf-051d-30ae-8fb8-47c60b8ea2fa"
        },
        "summary": {
          "excerpt": "Hillary Clinton has long been the favourite in prediction markets to win the US presidency in next month’s elections..."
        },
        "editorial": {
          "byline": "Adam Samson"
        }
      },
      {
        "aspectSet": "article",
        "modelVersion": "1",
        "id": "3b323e04-9b8b-11e6-8f9b-70e3cabccfae",
        "apiUrl": "https://api.ft.com/content/3b323e04-9b8b-11e6-8f9b-70e3cabccfae",
        "title": {
          "title": "Clinton needs more than competence to quell America’s anger"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T16:38:45Z",
          "lastPublishDateTime": "2016-10-28T19:05:26Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/3b323e04-9b8b-11e6-8f9b-70e3cabccfae"
        },
        "summary": {
          "excerpt": "...more polite than convinced, Hillary Clinton praised Mr Trump’s children, saying they reflected well on their father. But..."
        },
        "editorial": {
          "subheading": "Rage and gloom are not its citizens’ default setting, writes Simon Schama",
          "byline": "Simon Schama"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "09609623-6c2c-34be-8211-00fc4099936e",
        "apiUrl": "https://api.ft.com/content/09609623-6c2c-34be-8211-00fc4099936e",
        "title": {
          "title": "What’s “new and substantial” about Clinton emails?"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T18:30:36Z",
          "lastPublishDateTime": "2016-10-28T18:30:36Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/09609623-6c2c-34be-8211-00fc4099936e"
        },
        "summary": {
          "excerpt": "...newly discovered emails from Democratic presidential nominee Hillary Clinton. But just a month ago, he had rejected a..."
        },
        "editorial": {
          "byline": "Jessica Dye"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "7a6a6f38-1a57-326c-a64b-77be2aa7aebe",
        "apiUrl": "https://api.ft.com/content/7a6a6f38-1a57-326c-a64b-77be2aa7aebe",
        "title": {
          "title": "Haven assets rise as Clinton probe sparks market jitters"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T18:10:56Z",
          "lastPublishDateTime": "2016-10-28T18:10:56Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/7a6a6f38-1a57-326c-a64b-77be2aa7aebe"
        },
        "summary": {
          "excerpt": "Safe haven assets were on the rise on Friday after news that the FBI has opened a new investigation into Hillary..."
        },
        "editorial": {
          "byline": "Wataru Suzuki"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "4e99ba99-eb8b-3bbc-a381-a5f29d939ab5",
        "apiUrl": "https://api.ft.com/content/4e99ba99-eb8b-3bbc-a381-a5f29d939ab5",
        "title": {
          "title": "Wall St ‘fear gauge’ jumps 10% on new FBI Clinton probe"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T17:53:02Z",
          "lastPublishDateTime": "2016-10-28T17:53:02Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/4e99ba99-eb8b-3bbc-a381-a5f29d939ab5"
        },
        "summary": {
          "excerpt": "A closely watched measure of expected volatility in the US equity market shot up by almost 10 per cent as news that the..."
        },
        "editorial": {
          "byline": "Adam Samson"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "be624377-8ab9-372b-8e68-9001bae393fa",
        "apiUrl": "https://api.ft.com/content/be624377-8ab9-372b-8e68-9001bae393fa",
        "title": {
          "title": "FBI launches new investigation of Clinton emails"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T17:34:49Z",
          "lastPublishDateTime": "2016-10-28T17:34:49Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/be624377-8ab9-372b-8e68-9001bae393fa"
        },
        "summary": {
          "excerpt": "The FBI has told members of Congress it is investigating new emails discovered in connection with former Secretary of..."
        },
        "editorial": {
          "byline": "Financial Times"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "720bd5bf-1d5a-370b-885e-7865865f74c0",
        "apiUrl": "https://api.ft.com/content/720bd5bf-1d5a-370b-885e-7865865f74c0",
        "title": {
          "title": "Markets shaken as FBI opens new Clinton email probe"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T17:28:45Z",
          "lastPublishDateTime": "2016-10-28T17:28:45Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/720bd5bf-1d5a-370b-885e-7865865f74c0"
        },
        "summary": {
          "excerpt": "Talk about dropping a bombshell. With 11 days to go before the US elections, FBI director James Comey said on Friday..."
        },
        "editorial": {
          "byline": "Pan Kwan Yuk"
        }
      },
      {
        "aspectSet": "blogPost",
        "modelVersion": "1",
        "id": "bf047fb0-c8e1-3e88-a447-dfb7a8dd1cb1",
        "apiUrl": "https://api.ft.com/content/bf047fb0-c8e1-3e88-a447-dfb7a8dd1cb1",
        "title": {
          "title": "Michelle Obama and Hillary Clinton campaign in North Carolina"
        },
        "lifecycle": {
          "initialPublishDateTime": "2016-10-28T09:30:06Z",
          "lastPublishDateTime": "2016-10-28T09:30:06Z"
        },
        "location": {
          "uri": "https://www.ft.com/content/bf047fb0-c8e1-3e88-a447-dfb7a8dd1cb1"
        },
        "summary": {
          "excerpt": "U.S. first lady Michelle Obama embraces U.S. Democratic presidential candidate Hillary Clinton as they arrive at a..."
        },
        "editorial": {
          "byline": "Annabel Cook"
        }
      }
    ]
  }]
}

