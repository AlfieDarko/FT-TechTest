var request = require('supertest')
var chai = require('chai')
var expect = chai.expect
let app = require('../app.js')
describe('Homepage /', function () {
  it("Has the title 'FT News Aggregator'", function (done) {
    request(app)
      .get('/')
      // .expect(200)
      .expect(/FT News Aggregator/, done)
      // .expect(/ Headlines on the go /, done)
  })
})
