var request = require('supertest')
var chai = require('chai')
var expect = chai.expect
let app = require('../app.js')
let response = require('./helpers/response.js')
let nock = require('nock')
require('dotenv').config()

describe(' POST /articles', () => {
  beforeEach(() => {
    nock('http://localhost:3000/articles').post({
      queryString: 'Trump AND title:"Clinton"',
      queryContext: {
        curations: ['ARTICLES']
      }
    }).reply(200, {
      status: 200,
      response: response
    })
  })

  it('returns an object of artcles with the search parameter in the headlines', () => {
    postQuery = {
      queryString: 'Trump AND title:"Clinton"',
      queryContext: {
        curations: ['ARTICLES']
      }
    }
    request(app).get('/articles').end(function (err, res) {
      if (err) {
        console.log(err)
      } else {
        expect(res.body.status).to.equal(200)
        expect(res.body.response).to.equal(response)
      }
    })
  })
})
