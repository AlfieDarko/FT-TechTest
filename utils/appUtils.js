const axios = require('axios')

class AppUtils { 
  constructor(){
    this._searchQuery = ""
  }

  static getSearchBoxValue(){
    return this._searchQuery
  }

  static setSearchBoxValue(searchBox){
    this._searchQuery = searchBox
  }


  static searchQuery(currentPage) {
    const MAX_RESULTS = 20
    const QUERY_STRING = `title:  \"${this.getSearchBoxValue()}\"`
    
    function offsetByPageNumber(currentPage) {    
      let result = currentPage * 20
      return result
    }
      return {
      queryString: QUERY_STRING,
      queryContext: {
        'curations': ['ARTICLES']
      },
      resultContext: {
        facets: {
          names: ['subjects']
        },
        maxResults: MAX_RESULTS,
        offset: offsetByPageNumber(currentPage),
        aspects: ['title', 'lifecycle', 'location', 'summary', 'editorial']
      }
    }
  }

  static searchArticles(req, res, currentPage = 1) {
    const API_URL = `http://api.ft.com/content/search/v1?apiKey=${process.env.FT_API_KEY}`
    axios.post(
      API_URL, 
      this.searchQuery(currentPage)
    ).then(response => {
      
      let numberOfResults = response.data.results[0].indexCount
      if (numberOfResults === 0) {
        res.render('index', {
          title: 'No Results Found',
          search: req.body.searchBox
        })
      } else {
        res.render('index', {
          title: 'Results',
          articles: response.data.results[0].results.map(x => x),
          search: req.body.searchBox,
          pagination: {
            currentPage: currentPage
          }
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }

}

module.exports = AppUtils